# JSON Server

https://github.com/typicode/json-server

- 簡易 Restful API サーバー
- サーバのモックとして利用

## install

```
mkdir jsonserver

docker run \
  --rm \
  -it \
  -v jsonserver:/usr/local \
  node \
    npm install -g json-server
```

## 起動

```
tee jsonserver/db.json << EOF
{
  "posts": [
    { "id": 1, "title": "json-server", "author": "typicode" }
  ],
  "comments": [
    { "id": 1, "body": "some comment", "postId": 1 }
  ],
  "profile": { "name": "typicode" }
}
EOF
```

`--host` を指定しないと外部からアクセス不可

```
docker run \
  --name jsonserver1 \
  -u "node" \
  -d \
  -p 3000 \
  -v jsonserver:/usr/local \
  -v $(pwd)/jsonserver:/home/node/app \
  -w "/home/node/app" \
  --network=kong-net \
  node \
    json-server --watch db.json --host 0.0.0.0 --port 3000
```

```
docker run \
  --rm \
  --network=kong-net \
  node \
    curl -X GET \
      http://jsonserver1:3000/posts
```

```
docker run \
  --name jsonserver2 \
  -u "node" \
  -d \
  -p 3000 \
  -v jsonserver:/usr/local \
  -v $(pwd)/jsonserver:/home/node/app \
  -w "/home/node/app" \
  --network=kong-net \
  node \
    json-server --watch db.json --host 0.0.0.0 --port 3000
```
