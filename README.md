# kong

API Gateway の Kong の勉強用

https://konghq.com/

## install with docker

https://docs.konghq.com/install/docker/?_ga=2.229072028.1690299858.1591787914-1517790808.1591787914

```
docker network create kong-net

docker run -d --name kong-database \
               --network=kong-net \
               -p 5432:5432 \
               -e "POSTGRES_USER=kong" \
               -e "POSTGRES_DB=kong" \
               -e "POSTGRES_PASSWORD=kong" \
               postgres:9.6

docker run --rm \
     --network=kong-net \
     -e "KONG_DATABASE=postgres" \
     -e "KONG_PG_HOST=kong-database" \
     -e "KONG_PG_USER=kong" \
     -e "KONG_PG_PASSWORD=kong" \
     -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
     kong:latest kong migrations bootstrap

docker run -d --name kong \
     --network=kong-net \
     -e "KONG_DATABASE=postgres" \
     -e "KONG_PG_HOST=kong-database" \
     -e "KONG_PG_USER=kong" \
     -e "KONG_PG_PASSWORD=kong" \
     -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
     -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
     -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
     -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_LISTEN=0.0.0.0:8001, 0.0.0.0:8444 ssl" \
     -p 8000:8000 \
     -p 8443:8443 \
     -p 127.0.0.1:8001:8001 \
     -p 127.0.0.1:8444:8444 \
     kong:latest

curl -i http://localhost:8001/
```

## Getting Started

https://docs.konghq.com/getting-started-guide/latest/overview/

### Expose your Services with Kong Gateway

#### Add a Service

```
# mock1
curl -i -X POST http://localhost:8001/services \
 --data name=jsonserver1 \
 --data url='http://jsonserver1:3000'

curl -i http://localhost:8001/services/jsonserver1

# mock2
curl -i -X POST http://localhost:8001/services \
 --data name=jsonserver2 \
 --data url='http://jsonserver2:3000'

curl -i http://localhost:8001/services/jsonserver2
```

```
curl http://localhost:8001/services


curl -i -X DELETE http://localhost:8001/services/jsonserver1
curl -i -X DELETE http://localhost:8001/services/jsonserver2
```

#### Add a Route

```
# mock1
curl -i -X POST http://localhost:8001/services/jsonserver1/routes \
  --data 'paths[]=/mock1' \
  --data 'name=mocking'

curl -i -X GET http://localhost:8000/mock1
curl -i -X GET http://localhost:8000/mock1/posts

# mock2
curl -i -X POST http://localhost:8001/services/jsonserver2/routes \
  --data 'paths[]=/mock2' \
  --data 'name=mocking2'

curl -i -X GET http://localhost:8000/mock2
curl -i -X GET http://localhost:8000/mock2/posts
```

```
curl -i -X GET http://localhost:8001/routes
curl -i -X GET http://localhost:8001/services/jsonserver1/routes
curl -i -X GET http://localhost:8001/services/jsonserver2/routes


curl -i -X DELETE http://localhost:8001/services/jsonserver1/routes/mocking
curl -i -X DELETE http://localhost:8001/services/jsonserver2/routes/mocking2
```

### Secure your Services Using Authentication

#### Set up the Key Authentication Plugin

```
curl -X POST http://localhost:8001/routes/mocking2/plugins \
 --data name=key-auth

curl -i -X GET http://localhost:8000/mock2
```

#### Set up Consumers and Credentials

```
curl -i -X POST -d "username=consumer&custom_id=consumer" http://localhost:8001/consumers/

curl -i -X POST http://localhost:8001/consumers/consumer/key-auth -d 'key=apikey'
```

#### Validate Key Authentication

```
curl -i http://localhost:8000/mock2 -H 'apikey:apikey'
curl -i http://localhost:8000/mock2/posts -H 'apikey:apikey'
```

#### (Optional) Disable the plugin

```
curl -X GET http://localhost:8001/routes/mocking2/plugins/

id=`curl -X GET http://localhost:8001/routes/mocking2/plugins/ | sed -r 's/.*,"id":"([^"]+).*/\1/'`
curl -X PATCH http://localhost:8001/routes/mocking2/plugins/${id} \
 --data "enabled=false"

curl -i http://localhost:8000/mock2/posts
```
